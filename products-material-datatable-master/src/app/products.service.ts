import { Injectable } from "@angular/core";
import { HttpClient, HttpParams, HttpClientModule } from "@angular/common/http";
import { Product } from "./product";

import { Observable,Observer } from "rxjs";

import { Http, Response, Headers, RequestOptions } from '@angular/http';    
import 'rxjs/add/operator/map';  
import 'rxjs/add/operator/catch';  
import 'rxjs/add/operator/toPromise';  
import 'rxjs/add/observable/throw';  
import { map, retry, catchError } from "rxjs/operators";
import {  Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';






const productsUrl = "http://localhost:3000/reservas";

@Injectable({
  providedIn: "root"
})
export class ProductsService {
  private headers = new Headers({ 'Content-Type': 'application/json' });  
  // Define API
  apiURL = 'http://localhost:3000';
  
  // For Using Fake API by Using It's URL  
  constructor(private http: HttpClient) {}
   
  
  // To fill the Datatable for Default Table [Dummy Data]  
  public GetAllAlbums() {  
    return this.http.get(productsUrl)  
      .catch(this.handleError);  
  }
  
  getReserva(id: number): Observable<Product>{
    return this.http.get<Product>(productsUrl+id);
  }

 // To provide error description   
  private handleError(error: Response | any) {  
    console.error(error.message || error);  
    return Observable.throw(error.status);  
  }  

   // HttpClient API get() method => Fetch employees list
   getReservas(): Observable<Product> {
    return this.http.get<Product>(this.apiURL + '/reservas/')
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

    // HttpClient API get() method => Fetch employee
    getReserv(id:number): Observable<Product> {
      return this.http.get<Product>(this.apiURL + '/reservas/' + id)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
    }  

 // HttpClient API put() method => Update employee
 updateEmployee(id: number, reserva: Product): Observable<Product> {
  return this.http.put<Product>(this.apiURL + '/reservas/' + id, reserva)
  .pipe(
    retry(1),
    catchError(this.handleError)
  )
}


}
 