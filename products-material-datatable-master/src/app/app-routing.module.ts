import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyTableComponent } from './my-table/my-table.component';
import { EditComponent } from './edit/edit.component';


const routes: Routes = [
//{path: '', redirectTo: 'agenda', pathMatch: 'full' },
/* {path: 'agenda', children:
[
  {path: 'reserva', component: MyTableComponent },
  {path: 'editar/:id', component: EditComponent}
]}, */
//{ path: '', redirectTo: '/', pathMatch: 'full' },
{
  path: 'agenda',
  children: [
    {  path: 'reserva', component: MyTableComponent },
    { path: 'reserva/editar/:id', component: EditComponent },  
  ]
},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
