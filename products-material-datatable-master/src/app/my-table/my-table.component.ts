import { Component, OnInit, ViewChild, AfterViewInit } from "@angular/core";
import { MatPaginator, MatSort } from "@angular/material";
import { tap } from "rxjs/operators";
import { Product } from "../product";
import { ProductsService } from "../products.service";
import { MatTableDataSource } from '@angular/material';


import {MatIconModule} from '@angular/material/icon';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';  
import { Observable } from 'rxjs/Observable';  
import { Router } from '@angular/router';
 



@Component({
  selector: "app-my-table",
  templateUrl: "./my-table.component.html",
  styleUrls: ["./my-table.component.css"]
})
export class MyTableComponent implements OnInit {

  MyDataSource: any;  
  displayedColumns = ["id", "nombreLocal", "nombreSucursal", "fecha", "horaInicio", "horaFin",
"nombreCliente", "nombreEspecialidad", "nombreServicio", "nombreEmpleado","flagEstado", "flagAsistio",
"observacion", "accion"];  
  @ViewChild(MatPaginator) paginator: MatPaginator;  
  @ViewChild(MatSort) sort: MatSort;  
    
  constructor(private router: Router, public dataService: ProductsService) { }  
  
  ngOnInit() {  
    this.RenderDataTable();  
    //this.MyDataSource.sort = this.sort // which is viewchild sorting object  
  
  }  
 
  RenderDataTable() {  
    this.dataService.getReservas()  
      .subscribe(  
      res => {  
        this.MyDataSource = new MatTableDataSource();  
        this.MyDataSource.data = res;  
        this.MyDataSource.sort = this.sort;  
        this.MyDataSource.paginator = this.paginator;  
        console.log(this.MyDataSource.data);  
      },  
      error => {  
        console.log('There was an error while retrieving Albums !!!' + error);  
      });  
  }  


}
