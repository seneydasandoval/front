import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Product } from '../app/product';
@Injectable({
  providedIn: 'root'
})
export class EditServiceService {

  private basePath = 'http://localhost:3000/reservas/';

  constructor(private httpClient: HttpClient){}

  editar(reserva: Product): Observable<Product>{
      return this.httpClient.put<Product>(this.basePath + reserva.id, reserva);
  }
}
