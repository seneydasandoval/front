import { Component, OnInit } from '@angular/core';
import {  EventEmitter, Output, ViewChild } from '@angular/core';

import { Product } from '../product';
import { ActivatedRoute, Router } from '@angular/router';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { ProductsService } from '../products.service';
import { EditServiceService } from '../edit-service.service';





@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  public form: FormGroup;
  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private editarServicio: EditServiceService,
    private recibir: ProductsService,
    private router: Router,
    ) { }
    ngOnInit() {
      const id = +this.route.snapshot.paramMap.get('id'); //Se obtiene el id de la ruta 
      //Para el servidor
      this.getReservaById(id);


  }

  onSubmit() {
    // TODO: Use EventEmitter with form value
  console.log( this.form.value);
   if (this.form.valid) {
     this.editarServicio.editar(
      this.form.value).subscribe(
       data => this.editadoCorrectamente(data),
       error => this.editadoIncorrecto(error));
    }
  }
  editadoCorrectamente(data: Product){
    console.log('Editado Correctamente');
    console.log(data);
  }

  editadoIncorrecto(error){
    console.log('No se ha podido guardar los cambios. Error en el servidor!');
    console.log(error);
  }


  getReservaById(id: number) {
    this.recibir.getReserv(id).subscribe(
      respuesta => {
        this.cargarFormulario(respuesta);
        console.log(respuesta);
      },
      error_respuesta => {
        console.log('Ha ocurrido un error al intentar cargar los datos del postulante');
        console.log(error_respuesta);
      }
      );
  }


  cargarFormulario(reserva: Product){
    this.form = this.formBuilder.group({
      id: new FormControl(reserva.id),
      fecha: new FormControl(reserva.fecha),
      horaInicio: new FormControl(reserva.horaInicio),
      horaFin: new FormControl(reserva.horaFin),
      fechaHoraCreacion: new FormControl(reserva.fechaHoraCreacion),
      idCliente: new FormControl(reserva.idCliente),
      nombreCliente: new FormControl(reserva.nombreCliente),
      idEmpleado: new FormControl(reserva.idEmpleado),
      nombreEmpleado: new FormControl(reserva.nombreEmpleado),
      flagEstado: new FormControl(reserva.flagEstado),
      flagAsistio: new FormControl(reserva.flagAsistio),
      observacion: new FormControl(reserva.observacion),
      idSucursal: new FormControl(reserva.idSucursal),
      nombreSucursal: new FormControl(reserva.nombreSucursal),
      idLocal: new FormControl(reserva.idLocal),
      nombreLocal: new FormControl(reserva.nombreLocal),
      idServicio: new FormControl(reserva.idServicio),
      nombreServicio: new FormControl(reserva.nombreServicio),
      idEspecialidad: new FormControl(reserva.idEspecialidad),
      nombreEspecialidad: new FormControl(reserva.nombreEspecialidad),
    });
  }


  onclickBack(){
    this.router.navigate(['/agenda/reserva']);
  }


}
